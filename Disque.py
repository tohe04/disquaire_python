#python3

class Disque :

	"""
		Gestionnaire de disque musicaux.

	"""
	def __init__(self, artiste, nomAlbum, listeTitres):
		self.artiste = artiste
		self.nomAlbum = nomAlbum
		self.titres = listeTitres


	def afficherTitres(self):
		"""
			Affiche la liste des titres de l'album.
		"""
		for i in range(len(self.titres)):
			print(str(i+1)+'.', self.titres[i])


	def afficherAlbum(self):
		"""
			Affiche l'artiste, le nom de l'album et les différent titres de l'album
		"""
		print("Artiste :" self.artiste)
		print("Album :", self.nomAlbum)
		print("Titres :")
		self.afficherTitres()





